export const getServerSideProps = async () => {
  const res = await fetch('http://api:3000/ciclos')
  const { data: ciclos } = await res.json()
  return { props: { ciclos } }
}

export default function Home({ ciclos }) {
  return (
    <>
      <h1>Meu Ciclo de Estudos</h1>
      <pre>{JSON.stringify(ciclos, null, 2)}</pre>
    </>
  )
}
