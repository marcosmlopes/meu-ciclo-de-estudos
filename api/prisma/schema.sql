CREATE TABLE mce_usuarios (
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(100) NOT NULL,
  email VARCHAR(255) NOT NULL,
  senha_hash VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (email)
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE mce_ciclos_estudos (
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(255) NOT NULL,
  tempo_sessoes_pomodoro INT NOT NULL,
  tempo_descanso_pomodoro INT NOT NULL,
  id_usuario INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_ciclos_estudos_usuarios
    FOREIGN KEY (id_usuario)
    REFERENCES mce_usuarios (id)
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE mce_disciplinas (
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(255) NOT NULL,
  id_ciclo_estudos INT NOT NULL,
  minutos_estudo_diario INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_mce_disciplinas_ciclos_estudos
    FOREIGN KEY (id_ciclo_estudos)
    REFERENCES mce_ciclos_estudos (id)
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE mce_sessoes (
  id INT NOT NULL AUTO_INCREMENT,
  criado_em DATETIME NOT NULL DEFAULT NOW(),
  id_materia_estudada INT NOT NULL,
  id_ciclo_estudos INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_mce_sessoes_disciplinas
    FOREIGN KEY (id_materia_estudada)
    REFERENCES mce_disciplinas (id),
  CONSTRAINT fk_mce_sessoes_ciclos_estudos
    FOREIGN KEY (id_ciclo_estudos)
    REFERENCES mce_ciclos_estudos (id)
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
