const express = require('express')
const bodyParser = require('body-parser')
const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()
const app = express()

app.use(bodyParser.json())

app.get('/ciclos', async (req, res) => {
  const ciclos = await prisma.cicloEstudos.findMany({
    include: {
      usuario: true,
      disciplinas: true
    }
  })
  res.json({ data: ciclos })
})

app.listen(3000, () => console.log('API Server started at localhost:3000'))
