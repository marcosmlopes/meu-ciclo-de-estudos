FROM node:12.19.0

RUN mkdir /app
WORKDIR /app

COPY package.json yarn.lock* ./

RUN yarn install --frozen-lockfile

CMD ["yarn", "dev"]
